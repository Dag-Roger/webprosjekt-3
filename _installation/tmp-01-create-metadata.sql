CREATE DATABASE IF NOT EXISTS `login`;
USE login;

CREATE USER IF NOT EXISTS 'MainAdmin'@'localhost' IDENTIFIED BY 'PolkaDots';
GRANT ALL ON login.* TO 'MainAdmin'@'localhost';

CREATE TABLE IF NOT EXISTS `login`.`users` (
 `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'auto incrementing user_id of each user, unique index',
 `user_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s name, unique',
 `user_password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s password in salted and hashed format',
 `user_email` varchar(254) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s email, unique',
 `user_access_level` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'user''s access level (between 0 and 255, 255 = administrator)',
 `user_active` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'user''s activation status',
 `user_activation_hash` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'user''s email verification hash string',
 `user_password_reset_hash` char(40) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'user''s password reset code',
 `user_password_reset_timestamp` bigint(20) DEFAULT NULL COMMENT 'timestamp of the password reset request',
 `user_failed_logins` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'user''s failed login attemps',
 `user_last_failed_login` int(10) DEFAULT NULL COMMENT 'unix timestamp of last failed login attempt',
 `user_registration_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
 `user_registration_ip` varchar(39) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0.0.0.0',
 PRIMARY KEY (`user_id`),
 UNIQUE KEY `user_name` (`user_name`),
 UNIQUE KEY `user_email` (`user_email`)
) AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='user data';

CREATE TABLE IF NOT EXISTS `login`.`user_connections` (
 `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
 `user_id` int(11) unsigned NOT NULL,
 `user_rememberme_token` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
 `user_last_visit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
 `user_last_visit_agent` text COLLATE utf8_unicode_ci,
 `user_login_ip` varchar(39) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0.0.0.0',
 `user_login_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
 `user_login_agent` text COLLATE utf8_unicode_ci,
 PRIMARY KEY (`id`)
) AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='authenticated user data';

CREATE TABLE IF NOT EXISTS `login`.`suggestion_category` (
 `category_id`  INT AUTO_INCREMENT PRIMARY KEY,
 `category_name` varchar(64) NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS `login`.`suggestion_status` (
 `status_id`  INT AUTO_INCREMENT PRIMARY KEY,
 `status_name` varchar(64) NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS `login`.`suggestions` (
 `suggestion_id`  INT AUTO_INCREMENT PRIMARY KEY,
 `title` varchar(255) NOT NULL,
 `suggestion_text` TEXT NOT NULL,
 `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
 `updated_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
 `category` INT NOT NULL,
 `status` INT DEFAULT 1 NOT NULL,
 `author` int(11) unsigned NOT NULL,
 `over_max_reported` BOOLEAN DEFAULT 0,
 `post_anonymous` BOOLEAN DEFAULT 0,
 `votes` INT DEFAULT 0
);

CREATE TABLE IF NOT EXISTS `login`.`suggestion_reports` (
 `report_id`  INT AUTO_INCREMENT PRIMARY KEY,
 `suggestion` INT NOT NULL,
 `user` int(11) unsigned NOT NULL,
 `report_text` varchar(255) NOT NULL,
 `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL
);

CREATE TABLE IF NOT EXISTS `login`.`suggestion_user_votes` (
 `suggestion`  INT NOT NULL,
 `user` int(11) unsigned NOT NULL
);

-- Adding Composite Primary Key to suggestion_user_votes table
ALTER TABLE `login`.`suggestion_user_votes` ADD CONSTRAINT pk_users_voted PRIMARY KEY (`suggestion`, `user`);

-- Adding Foreign Keys
ALTER TABLE `login`.`suggestions` ADD CONSTRAINT fk_suggestions_users FOREIGN KEY (author) REFERENCES users(user_id);
ALTER TABLE `login`.`suggestions` ADD CONSTRAINT fk_suggestions_category FOREIGN KEY (category) REFERENCES suggestion_category(category_id);
ALTER TABLE `login`.`suggestions` ADD CONSTRAINT fk_suggestions_sugg_status FOREIGN KEY (status) REFERENCES suggestion_status(status_id);

ALTER TABLE `login`.`suggestion_reports` ADD CONSTRAINT fk_reports_suggestions FOREIGN KEY (suggestion) REFERENCES suggestions(suggestion_id);
ALTER TABLE `login`.`suggestion_reports` ADD CONSTRAINT fk_reports_users FOREIGN KEY (user) REFERENCES users(user_id);

ALTER TABLE `login`.`suggestion_user_votes` ADD CONSTRAINT fk_usrvotes_suggestions FOREIGN KEY (suggestion) REFERENCES suggestions(suggestion_id);
ALTER TABLE `login`.`suggestion_user_votes` ADD CONSTRAINT fk_usrvotes_users FOREIGN KEY (user) REFERENCES users(user_id);
