INSERT INTO `login`.`users`(user_name, user_password_hash, user_email, user_active)
  VALUES (
    'Ola Normann',
    'testpass',
    'onord@stud.ntnu.no',
    1
  );

INSERT INTO `login`.`users`(user_name, user_password_hash, user_email, user_active)
  VALUES (
    'Kari Karisenn',
    '$2y$10$CF4xHas7CyVbWEzG0DyuOuyq.0UstzAYkw56Vjec9M4Q1nUZQLrza', -- test123
    'dag.roger.e@gmail.com',
    1
  );

INSERT INTO `login`.`suggestion_category`(category_name) VALUES ('IT');
INSERT INTO `login`.`suggestion_category`(category_name) VALUES ('Drift');
INSERT INTO `login`.`suggestion_category`(category_name) VALUES ('Velferd');
INSERT INTO `login`.`suggestion_category`(category_name) VALUES ('Sit');
INSERT INTO `login`.`suggestion_category`(category_name) VALUES ('Studentparlamentet');

INSERT INTO `login`.`suggestion_status`(status_name) VALUES ('Startet');
INSERT INTO `login`.`suggestion_status`(status_name) VALUES ('Tatt videre');

INSERT INTO `login`.`suggestions`(title, suggestion_text, category, author, votes)
  VALUES (
    'Title 1',
    'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
    1,
    1,
    3
  );

INSERT INTO `login`.`suggestions`(title, suggestion_text, category, author, post_anonymous, votes)
  VALUES (
    'Title 2',
    'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using "Content here, content here", making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for "lorem ipsum" will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',
    2,
    2,
    1,
    25
  );

INSERT INTO `login`.`suggestions`(title, suggestion_text, category, author, over_max_reported)
  VALUES (
    'Title 3',
    'Fuck. Fuck. Fuck.
    Mother mother f**k. Mother mother f**k f**k. Mother f**k mother f**k.
    Noise noise noise.
    1 2 1 2 3 4
    Noise noise noise.
    Smokin weed, smokin weed.
    Doin coke, drinkin beers.
    Drinkin beers, beers beers.
    Rollin fatties, smokin blunts.
    Who smokes the blunts? We smoke the blunts.
    Rollin blunts and smokin um',
    2,
    2,
    1
  );

INSERT INTO `login`.`suggestion_reports`(suggestion, user, report_text)
  VALUES (
    2,
    1,
    'Lots of swearing.'
  );
