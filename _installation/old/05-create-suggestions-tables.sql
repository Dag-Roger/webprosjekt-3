CREATE TABLE IF NOT EXISTS `login`.`suggestion_category` (
 `category_id`  INT AUTO_INCREMENT PRIMARY KEY,
 `category_name` varchar(64) NOT NULL UNIQUE
) ENGINE=MyISAM;

CREATE TABLE IF NOT EXISTS `login`.`suggestion_status` (
 `status_id`  INT AUTO_INCREMENT PRIMARY KEY,
 `status_name` varchar(64) NOT NULL UNIQUE
) ENGINE=MyISAM;

CREATE TABLE IF NOT EXISTS `login`.`suggestions` (
 `suggestion_id`  INT AUTO_INCREMENT PRIMARY KEY,
 `title` varchar(255) NOT NULL,
 `suggestion_text` TEXT NOT NULL,
 `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
 `updated_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
 `category` INT NOT NULL,
 `status` INT DEFAULT 1 NOT NULL,
 `author` int(11) unsigned NOT NULL,
 `over_max_reported` BOOLEAN DEFAULT 0,
 `post_anonymous` BOOLEAN DEFAULT 0,
 CONSTRAINT fk_suggestions_users FOREIGN KEY (author) REFERENCES users(user_id),
 CONSTRAINT fk_suggestions_category FOREIGN KEY (category) REFERENCES suggestion_category(category_id)
) ENGINE=MyISAM;

CREATE TABLE IF NOT EXISTS `login`.`suggestion_reports` (
 `report_id`  INT AUTO_INCREMENT PRIMARY KEY,
 `suggestion` INT NOT NULL,
 `user` INT NOT NULL,
 `report_text` varchar(255) NOT NULL,
 `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
 CONSTRAINT fk_reports_suggestions FOREIGN KEY (suggestion) REFERENCES suggestions(suggestion_id),
 CONSTRAINT fk_reports_users FOREIGN KEY (user) REFERENCES users(user_id)
) ENGINE=MyISAM;
