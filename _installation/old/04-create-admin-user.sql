CREATE USER IF NOT EXISTS 'MainAdmin'@'localhost' IDENTIFIED BY 'PolkaDots';
GRANT ALL ON login.* TO 'MainAdmin'@'localhost';
