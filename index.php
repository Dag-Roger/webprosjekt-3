<?php
/**
 * A simple PHP Login Script / ADVANCED VERSION
 *
 * @link https://github.com/devplanete/php-login-advanced
 * @license http://opensource.org/licenses/MIT MIT License
 */

// load php-login class
require_once("PHPLogin.php");
require_once("config/mysqli_config.php");
// the login object will do all login/logout stuff automatically
// so this single line handles the entire login process.
$login = new PHPLogin();

include('views/_header.php');

// show the registration form
if (isset($_GET['register']) && ! $login->isRegistrationSuccessful() &&
   (ALLOW_USER_REGISTRATION || (ALLOW_ADMIN_TO_REGISTER_NEW_USER && $_SESSION['user_access_level'] == 255))) {
    include('views/register.php');

// show the request-a-password-reset or type-your-new-password form
} else if (isset($_GET['password_reset']) && ! $login->isPasswordResetSuccessful()) {
    if (isset($_REQUEST['user_name']) && isset($_REQUEST['verification_code']) && $login->isPasswordResetLinkValid()) {
        // reset link is correct: ask for the new password
        include("views/password_reset.php");
    } else {
        // no data from a password-reset-mail has been provided,
        // we show the request-a-password-reset form
        include('views/password_reset_request.php');
    }

// show the edit form to modify username, email or password
} else if (isset($_GET['edit']) && $login->isUserLoggedIn()) {
    include('views/edit.php');

// the user clicked the login button in the nav menu for guests
}  else if (isset($_GET['login']) && !$login->isUserLoggedIn()) {
      include('views/login.php');

// the user clicked the Min Side button in the nav menu for logged in users
}  else if (isset($_GET['minside']) && $login->isUserLoggedIn()) {
      include('views/minside.php');

//the user clicked the Forslag link in the nav menu
}  else if (isset($_GET['forslag'])) {
      include('views/forslag.php');

//the user clicked the New Forslag link in the forslag.php
}  else if (isset($_GET['nytt_forslag']) && $login->isUserLoggedIn()) {
      include('views/nytt_forslag.php');

//the user clicked the New Forslag link in the forslag.php, but isn't logged in
}  else if (isset($_GET['nytt_forslag'])) {
      include('views/login.php');

//the user clicked the Veiledning link in the nav menu
}  else if (isset($_GET['veiledning'])) {
      include('views/veiledning.php');

// the user is logged in, we show the Min Side page
} else if ($login->isUserLoggedIn()) {
    include('views/forslag.php');

// the user is not logged in, we show the login form
} else {
    include('views/forslag.php');
}

include('views/_footer.php');
