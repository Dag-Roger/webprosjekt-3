<?php

/**
 * Please note: we can use unencoded characters like ö, é etc here as we use the html5 doctype with utf8 encoding
 * in the application's header (in views/_header.php). To add new languages simply copy this file,
 * and create a language switch in your root files.
 */

// login & registration classes
define("MESSAGE_ACCOUNT_NOT_ACTIVATED", "<div class='alert alert-danger'>Din bruker er ikke aktivert enda. Vennligst klikk på bekreftelseslenken tilsendt på epost.");
define("MESSAGE_CAPTCHA_WRONG", "<div class='alert alert-danger'>Captcha tegnene ble skrevet inn feil!");
define("MESSAGE_COOKIE_INVALID", "<div class='alert alert-danger'>Ugyldig cookie");
define("MESSAGE_DATABASE_ERROR", "<div class='alert alert-danger'>Database tilkoblings problem.");
define("MESSAGE_EMAIL_ALREADY_EXISTS", "<div class='alert alert-danger'>Denne e-postadressen har allerede blitt registrert. Gå til \"<a href='?password_reset'>glemt passord</a>\" siden, dersom du ikke husker ditt innloggingspassord.");
define("MESSAGE_EMAIL_CHANGE_FAILED", "<div class='alert alert-danger'>Beklager, noe gikk galt med endring av e-post.");
define("MESSAGE_EMAIL_CHANGED_SUCCESSFULLY", "<div class='alert alert-success'>Din e-postadresse har nå blitt endret. Din nye e-postaddresse er ");
define("MESSAGE_EMAIL_EMPTY", "<div class='alert alert-danger'>E-post kan ikke være tom");
define("MESSAGE_EMAIL_INVALID", "<div class='alert alert-danger'>Din e-postadresse har blitt skrevet inn feil.");
define("MESSAGE_EMAIL_SAME_LIKE_OLD_ONE", "<div class='alert alert-danger'>Beklager, e-postadressen er den samme som den gamle. Vennligst velg en annen.");
define("MESSAGE_EMAIL_TOO_LONG", "<div class='alert alert-danger'>E-postadressen kan ikke være lenger enn 254 tegn.");
define("MESSAGE_LINK_PARAMETER_EMPTY", "<div class='alert alert-danger'>Lenke parameter data er tom.");
define("MESSAGE_LOGGED_OUT", "<div class='alert alert-success'>Du er nå logget ut, takk for besøket.<meta http-equiv='refresh' content='5; url=.'' />");
// The "login failed"-message is a security improved feedback that doesn't show a potential attacker if the user exists or not
define("MESSAGE_LOGIN_FAILED", "<div class='alert alert-danger'>Feil brukernavn eller passord");
define("MESSAGE_OLD_PASSWORD_WRONG", "<div class='alert alert-danger'>Ditt GAMLE passord er skrevet inn feil");
define("MESSAGE_PASSWORD_BAD_CONFIRM", "<div class='alert alert-danger'>Gjentatt passord er skrevet inn feil");
define("MESSAGE_PASSWORD_CHANGE_FAILED", "<div class='alert alert-danger'>Huff, noe gikk galt med ditt bytte av passord.");
define("MESSAGE_PASSWORD_CHANGED_SUCCESSFULLY", "<div class='alert alert-success'>Passord endret!");
define("MESSAGE_PASSWORD_EMPTY", "<div class='alert alert-danger'>Passord felt kan ikke være tomt");
define("MESSAGE_PASSWORD_RESET_MAIL_FAILED", "<div class='alert alert-danger'>Beklager, passord nullstillings-posten ble ikke sendt. Feilmelding: ");
define("MESSAGE_PASSWORD_RESET_MAIL_SUCCESSFULLY_SENT", "<div class='alert alert-success'>Vi har nå sendt en e-post, med en lenke du må klikke på for å lage nytt passord til brukeren din.");
define("MESSAGE_PASSWORD_TOO_SHORT", "<div class='alert alert-danger'>Passordet må ha en minimum lengde av 6 tegn.");
define("MESSAGE_PASSWORD_WRONG", "<div class='alert alert-danger'>Feil passord. Prøv på nytt.");
define("MESSAGE_PASSWORD_WRONG_3_TIMES", "<div class='alert alert-danger'>Du har skrevet inn passordet ditt feil 3 ganger. Av sikkerhetsmessige årsaker vennligst vent i 30 sekunder før du prøver på nytt.");
define("MESSAGE_REGISTRATION_ACTIVATION_NOT_SUCCESSFUL", "<div class='alert alert-danger'>Beklager, bekreftelseslenken er skrevet inn feil. Prøv å skriv inn lenken pånytt.");
define("MESSAGE_REGISTRATION_ACTIVATION_SUCCESSFUL", "<div class='alert alert-success'>Brukeren din er aktivert! Du kan nå logge inn!");
define("MESSAGE_REGISTRATION_FAILED", "<div class='alert alert-danger'>Beklager, registreringen mislyktes. Prøv på nytt <a href='?register'>her</a>.");
define("MESSAGE_RESET_LINK_HAS_EXPIRED", "<div class='alert alert-danger'>Beklager, bekreftelseslenken din har utløpt. Bekreftelses e-post må svares på innen en time. Bestill ny bekreftelseslink <a href='?register'>her</a>");
define("MESSAGE_VERIFICATION_MAIL_ERROR", "<div class='alert alert-danger'>Beklager, noe gikk galt. Klarer ikke å sende deg bekreftelses-epost. Din bruker har IKKE blitt laget");
define("MESSAGE_VERIFICATION_MAIL_NOT_SENT", "<div class='alert alert-danger'>Bekreftelsesmail ikke sent! Feilmelding: ");
define("MESSAGE_VERIFICATION_MAIL_SENT", "<div class='alert alert-success'>Bruker registrert! For å aktivere din bruker, klikk på bekreftelseslink tilsendt på e-post.");
define("MESSAGE_USER_DOES_NOT_EXIST", "<div class='alert alert-danger'>Beklager, ingen bruker med denne e-postadressen eksisterer,");
define("MESSAGE_USERNAME_BAD_LENGTH", "<div class='alert alert-danger'>Brukernavnet kan ikke være kortere enn 2 tegn, eller lenger enn 64 tegn.");
define("MESSAGE_USERNAME_CHANGE_FAILED", "<div class='alert alert-danger'>Beklager, endringen av brukernavn mislyktes");
define("MESSAGE_USERNAME_CHANGED_SUCCESSFULLY", "<div class='alert alert-success'>Ditt brukernavn har nå blitt endret. Ditt nye brukernavn er ");
define("MESSAGE_USERNAME_EMPTY", "<div class='alert alert-danger'>Brukernavn feltet var tomt");
define("MESSAGE_USERNAME_EXISTS", "<div class='alert alert-danger'>Beklager, dette brukernavnet eksiterer allerede, vennligst velg et nytt.");
define("MESSAGE_USERNAME_INVALID", "<div class='alert alert-danger'>Brukernavnet kan kun inneholde bokstavene a-z og tall. Brukernavnet kan ikke være kortere enn 2 tegn, eller lenger enn 64 tegn.");
define("MESSAGE_USERNAME_SAME_LIKE_OLD_ONE", "<div class='alert alert-danger'>Brukernavnet ditt er det samme som det gamle, vennligst fyll inn et nytt ett.");

// views
define("WORDING_BACK_TO_LOGIN", "Tilbake til logg inn");
define("WORDING_CHANGE_EMAIL", "Bytt email");
define("WORDING_CHANGE_PASSWORD", "Bytt passord");
define("WORDING_CHANGE_USERNAME", "Bytt brukernavn");
define("WORDING_CURRENTLY", "nåværende");
define("WORDING_EDIT_USER_DATA", "Endre brukerinformasjon");
define("WORDING_EDIT_YOUR_CREDENTIALS", "Du er logget inn og kan endre dine legitimasjoner her");
define("WORDING_FORGOT_MY_PASSWORD", "Jeg har glemt mitt passord");
define("WORDING_LOGIN", "Logg inn");
define("WORDING_LOGOUT", "Logg ut");
define("WORDING_NEW_EMAIL", "Ny email");
define("WORDING_NEW_PASSWORD", "Nytt passord");
define("WORDING_NEW_PASSWORD_REPEAT", "Gjenta nytt passord");
define("WORDING_NEW_USERNAME", "Nytt brukernavn (brukernavn kan ikke være tomt og må være azAZ09 og 2-64 tegn)");
define("WORDING_OLD_PASSWORD", "Ditt GAMLE passord");
define("WORDING_PASSWORD", "Passord");
define("WORDING_PROFILE_PICTURE", "Ditt profilbilde (fra gravatar):");
define("WORDING_REGISTER", "Registrer");
define("WORDING_REGISTER_NEW_ACCOUNT", "Registrer ny bruker");
define("WORDING_REGISTRATION_CAPTCHA", "Vennligst skriv inn disse tegnene");
define("WORDING_REGISTRATION_EMAIL", "Brukerens epost (vennligst skriv inn en ekte epost, du vil motta en bekreftelsesepost med en aktiveringslink)");
define("WORDING_REGISTRATION_PASSWORD", "Passord (min. 6 tegn!)");
define("WORDING_REGISTRATION_PASSWORD_REPEAT", "Gjenta passord");
define("WORDING_REGISTRATION_USERNAME", "Brukernavn (bare bokstaver og nummere, 2 til 64 tegn)");
define("WORDING_REMEMBER_ME", "Hold meg logget inn");
define("WORDING_REQUEST_PASSWORD_RESET", "Be om å tilbakestille passord.<br/>Skriv inn ditt brukernavn eller email, og du vil motta en epost med instruksjoner:");
define("WORDING_RESET_PASSWORD", "Tilbakestill mitt passord");
define("WORDING_SUBMIT_NEW_PASSWORD", "Send inn nytt passord");
define("WORDING_USERNAME", "Brukernavn");
define("WORDING_YOU_ARE_LOGGED_IN_AS", "Du er logget inn som ");

define("WORDING_USER_EMAIL", "NTNU e-post");

// Nytt underskriftskampanjestekst / New Suggestion
define("MESSAGE_NO_TITLE", "<p>Ingen tittel ble skrevet inn.</p>");
define("MESSAGE_TITLE_LENGTH", "<p>Tittel må være mellom 5 og 120 bokstaver.</p>");
define("MESSAGE_NO_SUGGESTION_TEXT", "<p>Ingen kampanjetekst ble skrevet inn.</p>");
define("MESSAGE_SUGGESTION_LENGTH", "<p>Kampanjesteksten må være lengre enn 150 bokstaver.</p>");
define("MESSAGE_CORRECT_DEPARTMENT_CONTACTED", "Har du kontaktet riktig avdeling angående denne kampanjen?");
define("MESSAGE_ANONYMOUS_POSTING", "Ønsker du å være anonym?");
define("MESSAGE_TECHNICAL_DB_PROBLEM", "Vi har dessverre tekniske problemer. Vennligst prøv igjen ved en senere annledning.");
define("MESSAGE_NEW_SUGGESTION", "Lag kampanje");
define("WORDING_CHOOSE_CATEGORY", "Velg kategori:");
define("MESSAGE_SUGGESTION_SAVED", "Kampanjen ble lagret.");
define("WORDING_SUGGESTION_TITLE", "Tittel");
define("WORDING_SUGGESTION_TEXT", "Kampanjetekst");
define("WORDING_SUGGESTION_SUBMIT", "Lag kampanje");

// Alle kampanjer / Suggestion
define("MESSAGE_SUGGESTION_AUTHOR", "Du er forfatteren");
define("MESSAGE_NO_SUGGESTIONS", "Det er for øyeblikket ingen underskriftskampanjer.<br>");
define("WORDING_SUGGESTION_BY", "Av ");
define("WORDING_SUGGESTION_BY_ANONYMOUS", "Av Anonym");
define("WORDING_SUGGESTION_VOTES", "Stemmer");
define("WORDING_SUGGESTION_HEADER", "Alle kampanjer");
define("WORDING_ADD_NEW_SUGGESTION", "Lag kampanje");
define("WORDING_VOTE_BUTTON", "Stem");
define("WORDING_UNVOTE_BUTTON", "Fjern stemme");
define("WORDING_SEARCH_PLACEHOLDER", "Søk");
define("MESSAGE_SUCCESS", "(Suksess)");

// Min side / My page
define("MESSAGE_NO_USER_SUGGESTIONS", "<p>Du har for øyeblikket ingen kampanjer</p><br>");
define("MESSAGE_YOUR_SUGGESTIONS", "Mine kampanjer");
define("WORDING_YOUR_SUGGESTIONS_AUTHOR", "deg");

// Nav_guest / nav_loggedin
define("WORDING_NAVIGATION_SUGGESTIONS", "Alle kampanjer");
define("WORDING_NAVIGATION_GUIDANCE", "Informasjon");
define("WORDING_NAVIGATION_REGISTER_LOGIN", "Registrer / logg inn");

define("WORDING_NAVIGATION_MY_PAGE", "Mine kampanjer");
define("WORDING_NAVIGATION_LOGOUT", "Logg ut");

// Veiledning / Departements_ovrview
define("WORDING_DEPARTMENT_TITLE_HUSDRIFT", "Husdrift");
define("WORDING_DEPARTMENT_TITLE_STUDENTPARLAMENTET", "Studentparlamentet");
define("WORDING_DEPARTMENT_TITLE_IT", "IT");
define("WORDING_DEPARTMENT_TITLE_VELFERD", "Velferd");
define("WORDING_DEPARTMENT_TITLE_SIT", "SIT");
define("WORDING_DEPARTMENT_CONTACT", "Kontakt");

define("WORDING_HUSDRIFT_INFO", "Alle feil, behov og bestillinger av arbeid til driftsavdelingen, skal meldes til e-vaktmester. <br>
Eksempler:");
define("WORDING_HUSDRIFT_LIST", "
  <li>Skifte lyspære</li>
  <li>Henge opp bilder, hyller</li>
  <li>Behov for brannslukkingsutstyr eller feil på brannslukkingsutstyr</li>
  <li>Små reparasjonsbehov (dørpumpe, dryppende kran, slitt gulvbelegg, radiator)</li>
  <li>Manglende toalettpapir eller såpe på toaletter</li>
  <li>For kaldt - for varmt inne</li>
  <li>Snømåking</li>
  <li>Rydding utendørs</li>
  <li>Renhold</li>");
define("WORDING_HUSDRIFT_CONTACT", "For å melde inn saker til husdrift/e-vaktmester, benytt deg av lenken under: <br>");

define("WORDING_STUDENTPARLAMENTET_INFO",
    "Studentparlamentet er studentenes høyeste interesseorgan ved NTNU i
    Gjøvik. Er det saker studenten ønsker å ta opp som gjelder skolen eller
    velferd, er det de man skal henvende seg til. De har møter med skolens
    ledelse ved jevne mellomrom hvor de tar opp saker fra studentene.
    Studentparlamentet har kontor i kantinen og kontortid i lunsjen mandag
    til fredag, men om man ser noen av medlemmene er det bare å spørre i vei.");
define("WORDING_STUDENTPARLAMENTET_CONTACT",
  "<p>
      Kontor i kantina i G-bygget, rom G157 <br>
      Åpent 11:15 - 13:00 mandag til fredag
    </p>
    <p>
      Send epost for studentpolitiske saker: <br>
      <a href='mailto:sp@gjøvik.ntnu.no'>sp@gjovik.ntnu.no</a>
      <br>
      tlf: 61 13 51 71
    </p>
    <p>
      Studentparlamentet ved NTNU i Gjøvik
      <br>
      Postboks 191
      <br>
      2802 Gjøvik
    </p>");

define("WORDING_IT_INFO",
  "Den sentrale IT-avdelingen ved NTNU blir kalt NTNU IT. Sentralt for deres
  virksomhet er spisskompetanse på utviklig, drift og feilhåndtering
  innnefor nettverk, serverdrift og applikasjonsdrift. Deres hovedoppgaver
  gjelder drift av felles- og basistjenster for NTNU, utvikle web-baserte
  løsninger for NTNU og være rådgiver til organisasjonen på IT-relaterte
  spørsmål.");
define("WORDING_IT_CONTACT",
  "<p>
    Henvendelse i skranke: <br>
    Kjelleren i K-bygget
  </p>
  <p>
    Send epost for IT-relaterte saker: <br>
    <a href='mailto:orakel@gjøvik.ntnu.no'>orakel@gjovik.ntnu.no</a> <br>
    tlf: 73 59 15 00
  </p>");

define("WORDING_VELFERD_INFO",
  "<p>
    Velferdsutvalget er et rådgivende organ som innstiller til Velferdstinget
    i Gjøvik, Ålsesund og Trondheim. Velferdstinget skal forvalte
    studentvelferden til alle studenter som betaler semesteravgift til
    Studentsamskipnaden i Gjøvik, Ålesund og Trondheim. Velferdsutvalget består
    av seks (6) studenter, valgt av SR-FI og Studentparlamentet ved NTNU
    Gjøvik. Disse studentene har direktekontakt med Studentsamskipnaden,
    kommunen og Velferdstinget sentralt og skal være represantivt for alle
    studentene i Gjøvik
  </p>
  <p>
    Velferdsutvalget har i mandat å representere Gjøvikstudentenes syn i alle
    velferdspolitiske saker. Velferdsuytvalget skal ivareta studentenes velferd
    i Gjøvik, da gjennom poitisk påvirkning, samarbeid med Sit og kontinuerlig
    videreutvikling av tilbud for studentene.
  </p>
  <p>
    Velferdsutvalget skal ta opp saker som angår den generelle velferden til
    studentene i Gjøvik. Dette er alle saker som angår studentlivet utenfor
    studiet. Eksempler på dette kan være saker som omhandler studentidret,
    kollektivtransport eller studentboliger.
  </p>");
define("WORDING_VELFERD_CONTACT",
  "Send epost for velferdsrelaterte saker: <br>
  <a href='mailto:au-gjovik@velferdstinget.no'>au-gjovik@velferdstinget.no</a>");

define("WORDING_SIT_INFO",
  "<p>
    Sit skal bidra til at studentene trives, og at flere ønsker å studere i Gjøvik. De skal utvikle attraktive velferdstilbud og varer og tjenester som studentene har behov for. De kan engasjerer seg i ekstern forretningsvirksomhet når dette bidrar til økt studentvelferd. Engasjerte ansatte, støtte til studentenes frivillighetskultur og et tett samarbeid med studentene og utdanningsinstitusjonene skal være grunnpilarene i deres arbeid.
  </p>
  <p>
    Sit er organisert i tre utøvende enheter: Sit Bolig, Sit Kafe og Sit Velferd, i tillegg til administrative fellestjenester.
    De har også datterselskaper og tilknyttede selskaper.
  </p>");
define("WORDING_SIT_CONTACT",
  "<p>
    Sentralbord <br>
    E-post: <a href='mailto:sit@sit.no'>sit@sit.no</a> <br>
    Åpent mandag–fredag 08.30–15.00
  </p>
  <p>
    Sit Bolig <br>
    E-post: <a href='mailto:sit@sit.no'>bolig@sit.no</a>
  </p>
  <p>
    Sit Kafe <br>
    E-post: <a href='mailto:kafe@sit.no'>kafe@sit.no</a> <br>
    Tlf: 73 59 32 50
  </p>
  <p>
    Sit Velferd <br>
    E-post: <a href='mailto:helse@sit.no'>helse@sit.no</a>
  </p>");
