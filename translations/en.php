<?php

/**
 * Please note: we can use unencoded characters like ö, é etc here as we use the html5 doctype with utf8 encoding
 * in the application's header (in views/_header.php). To add new languages simply copy this file,
 * and create a language switch in your root files.
 */

// login & registration classes
define("MESSAGE_ACCOUNT_NOT_ACTIVATED", "Your account is not activated yet. Please click on the confirm link in the mail.");
define("MESSAGE_CAPTCHA_WRONG", "Captcha was wrong!");
define("MESSAGE_COOKIE_INVALID", "Invalid cookie");
define("MESSAGE_DATABASE_ERROR", "Database connection problem.");
define("MESSAGE_EMAIL_ALREADY_EXISTS", "This email address is already registered. Please use the \"I forgot my password\" page if you don't remember it.");
define("MESSAGE_EMAIL_CHANGE_FAILED", "Sorry, your email changing failed.");
define("MESSAGE_EMAIL_CHANGED_SUCCESSFULLY", "Your email address has been changed successfully. New email address is ");
define("MESSAGE_EMAIL_EMPTY", "Email cannot be empty");
define("MESSAGE_EMAIL_INVALID", "Your email address is not in a valid email format");
define("MESSAGE_EMAIL_SAME_LIKE_OLD_ONE", "Sorry, that email address is the same as your current one. Please choose another one.");
define("MESSAGE_EMAIL_TOO_LONG", "Email cannot be longer than 254 characters");
define("MESSAGE_LINK_PARAMETER_EMPTY", "Empty link parameter data.");
define("MESSAGE_LOGGED_OUT", "You have been logged out.");
// The "login failed"-message is a security improved feedback that doesn't show a potential attacker if the user exists or not
define("MESSAGE_LOGIN_FAILED", "Login failed.");
define("MESSAGE_OLD_PASSWORD_WRONG", "Your OLD password was wrong.");
define("MESSAGE_PASSWORD_BAD_CONFIRM", "Password and password repeat are not the same");
define("MESSAGE_PASSWORD_CHANGE_FAILED", "Sorry, your password changing failed.");
define("MESSAGE_PASSWORD_CHANGED_SUCCESSFULLY", "Password successfully changed!");
define("MESSAGE_PASSWORD_EMPTY", "Password field was empty");
define("MESSAGE_PASSWORD_RESET_MAIL_FAILED", "Password reset mail NOT successfully sent! Error: ");
define("MESSAGE_PASSWORD_RESET_MAIL_SUCCESSFULLY_SENT", "Password reset mail successfully sent!");
define("MESSAGE_PASSWORD_TOO_SHORT", "Password has a minimum length of 6 characters");
define("MESSAGE_PASSWORD_WRONG", "Wrong password. Try again.");
define("MESSAGE_PASSWORD_WRONG_3_TIMES", "You have entered an incorrect password 3 or more times already. Please wait 30 seconds to try again.");
define("MESSAGE_REGISTRATION_ACTIVATION_NOT_SUCCESSFUL", "Sorry, no such id/verification code combination here...");
define("MESSAGE_REGISTRATION_ACTIVATION_SUCCESSFUL", "Activation was successful! You can now log in!");
define("MESSAGE_REGISTRATION_FAILED", "Sorry, your registration failed. Please go back and try again.");
define("MESSAGE_RESET_LINK_HAS_EXPIRED", "Your reset link has expired. Please use the reset link within one hour.");
define("MESSAGE_VERIFICATION_MAIL_ERROR", "Sorry, we could not send you an verification mail. Your account has NOT been created.");
define("MESSAGE_VERIFICATION_MAIL_NOT_SENT", "Verification Mail NOT successfully sent! Error: ");
define("MESSAGE_VERIFICATION_MAIL_SENT", "Your account has been created successfully and we have sent you an email. Please click the VERIFICATION LINK within that mail.");
define("MESSAGE_USER_DOES_NOT_EXIST", "This user does not exist");
define("MESSAGE_USERNAME_BAD_LENGTH", "Username cannot be shorter than 2 or longer than 64 characters");
define("MESSAGE_USERNAME_CHANGE_FAILED", "Sorry, your chosen username renaming failed");
define("MESSAGE_USERNAME_CHANGED_SUCCESSFULLY", "Your username has been changed successfully. New username is ");
define("MESSAGE_USERNAME_EMPTY", "Username field was empty");
define("MESSAGE_USERNAME_EXISTS", "Sorry, that username is already taken. Please choose another one.");
define("MESSAGE_USERNAME_INVALID", "Username does not fit the name scheme: only a-Z and numbers are allowed, 2 to 64 characters");
define("MESSAGE_USERNAME_SAME_LIKE_OLD_ONE", "Sorry, that username is the same as your current one. Please choose another one.");

// views
define("WORDING_BACK_TO_LOGIN", "Back to Login Page");
define("WORDING_CHANGE_EMAIL", "Change email");
define("WORDING_CHANGE_PASSWORD", "Change password");
define("WORDING_CHANGE_USERNAME", "Change username");
define("WORDING_CURRENTLY", "currently");
define("WORDING_EDIT_USER_DATA", "Edit user data");
define("WORDING_EDIT_YOUR_CREDENTIALS", "You are logged in and can edit your credentials here");
define("WORDING_FORGOT_MY_PASSWORD", "I forgot my password");
define("WORDING_LOGIN", "Log in");
define("WORDING_LOGOUT", "Log out");
define("WORDING_NEW_EMAIL", "New email");
define("WORDING_NEW_PASSWORD", "New password");
define("WORDING_NEW_PASSWORD_REPEAT", "Repeat new password");
define("WORDING_NEW_USERNAME", "New username (username cannot be empty and must be azAZ09 and 2-64 characters)");
define("WORDING_OLD_PASSWORD", "Your OLD Password");
define("WORDING_PASSWORD", "Password");
define("WORDING_PROFILE_PICTURE", "Your profile picture (from gravatar):");
define("WORDING_REGISTER", "Register");
define("WORDING_REGISTER_NEW_ACCOUNT", "Register new account");
define("WORDING_REGISTRATION_CAPTCHA", "Please enter these characters");
define("WORDING_REGISTRATION_EMAIL", "User's email (please provide a real email address, you'll get a verification mail with an activation link)");
define("WORDING_REGISTRATION_PASSWORD", "Password (min. 6 characters!)");
define("WORDING_REGISTRATION_PASSWORD_REPEAT", "Password repeat");
define("WORDING_REGISTRATION_USERNAME", "Username (only letters and numbers, 2 to 64 characters)");
define("WORDING_REMEMBER_ME", "Keep me logged in");
define("WORDING_REQUEST_PASSWORD_RESET", "Request a password reset.<br/>Enter your username or email, and you'll get a mail with instructions:");
define("WORDING_RESET_PASSWORD", "Reset my password");
define("WORDING_SUBMIT_NEW_PASSWORD", "Submit new password");
define("WORDING_USERNAME", "Username");
define("WORDING_YOU_ARE_LOGGED_IN_AS", "You are logged in as ");


define("WORDING_USER_EMAIL", "NTNU e-post");

// Nytt underskriftskampanjestekst / New Suggestion
define("MESSAGE_NO_TITLE", "<p>Ingen tittel ble skrevet inn.</p>");
define("MESSAGE_TITLE_LENGTH", "<p>Tittel må være mellom 5 og 120 bokstaver.</p>");
define("MESSAGE_NO_SUGGESTION_TEXT", "<p>Ingen kampanjetekst ble skrevet inn.</p>");
define("MESSAGE_SUGGESTION_LENGTH", "<p>Kampanjesteksten må være lengre enn 150 bokstaver.</p>");
define("MESSAGE_CORRECT_DEPARTMENT_CONTACTED", "Har du kontaktet riktig avdeling angående denne kampanjen?");
define("MESSAGE_ANONYMOUS_POSTING", "Ønsker du å være anonym?");
define("MESSAGE_TECHNICAL_DB_PROBLEM", "Vi har dessverre tekniske problemer. Vennligst prøv igjen ved en senere annledning.");
define("MESSAGE_NEW_SUGGESTION", "Lag kampanje");
define("WORDING_CHOOSE_CATEGORY", "Velg kategori:");
define("MESSAGE_SUGGESTION_SAVED", "Kampanjen ble lagret.");
define("WORDING_SUGGESTION_TITLE", "Tittel");
define("WORDING_SUGGESTION_TEXT", "Kampanjetekst");
define("WORDING_SUGGESTION_SUBMIT", "Lag kampanje");

// Alle kampanjer / Suggestion
define("MESSAGE_SUGGESTION_AUTHOR", "Du er forfatteren");
define("MESSAGE_NO_SUGGESTIONS", "Det er for øyeblikket ingen underskriftskampanjer.<br>");
define("WORDING_SUGGESTION_BY", "Av ");
define("WORDING_SUGGESTION_BY_ANONYMOUS", "Av Anonym");
define("WORDING_SUGGESTION_VOTES", "Stemmer");
define("WORDING_SUGGESTION_HEADER", "Alle kampanjer");
define("WORDING_ADD_NEW_SUGGESTION", "Lag kampanje");
define("WORDING_VOTE_BUTTON", "Stem");
define("WORDING_UNVOTE_BUTTON", "Fjern stemme");
define("WORDING_SEARCH_PLACEHOLDER", "Søk");
define("MESSAGE_SUCCESS", "(Suksess)");

// Min side / My page
define("MESSAGE_NO_USER_SUGGESTIONS", "<p>Du har for øyeblikket ingen kampanjer</p><br>");
define("MESSAGE_YOUR_SUGGESTIONS", "Mine kampanjer");
define("WORDING_YOUR_SUGGESTIONS_AUTHOR", "deg");

// Nav_guest / nav_loggedin
define("WORDING_NAVIGATION_SUGGESTIONS", "Alle kampanjer");
define("WORDING_NAVIGATION_GUIDANCE", "Informasjon");
define("WORDING_NAVIGATION_REGISTER_LOGIN", "Registrer / logg inn");

define("WORDING_NAVIGATION_MY_PAGE", "Mine kampanjer");
define("WORDING_NAVIGATION_LOGOUT", "Logg ut");

// Veiledning / Departements_ovrview
define("WORDING_DEPARTMENT_TITLE_HUSDRIFT", "Husdrift");
define("WORDING_DEPARTMENT_TITLE_STUDENTPARLAMENTET", "Studentparlamentet");
define("WORDING_DEPARTMENT_TITLE_IT", "IT");
define("WORDING_DEPARTMENT_TITLE_VELFERD", "Velferd");
define("WORDING_DEPARTMENT_TITLE_SIT", "SIT");
define("WORDING_DEPARTMENT_CONTACT", "Kontakt");

define("WORDING_HUSDRIFT_INFO", "Alle feil, behov og bestillinger av arbeid til driftsavdelingen, skal meldes til e-vaktmester. <br>
Eksempler:");
define("WORDING_HUSDRIFT_LIST", "
  <li>Skifte lyspære</li>
  <li>Henge opp bilder, hyller</li>
  <li>Behov for brannslukkingsutstyr eller feil på brannslukkingsutstyr</li>
  <li>Små reparasjonsbehov (dørpumpe, dryppende kran, slitt gulvbelegg, radiator)</li>
  <li>Manglende toalettpapir eller såpe på toaletter</li>
  <li>For kaldt - for varmt inne</li>
  <li>Snømåking</li>
  <li>Rydding utendørs</li>
  <li>Renhold</li>");
define("WORDING_HUSDRIFT_CONTACT", "For å melde inn saker til husdrift/e-vaktmester, benytt deg av lenken under: <br>");

define("WORDING_STUDENTPARLAMENTET_INFO",
    "Studentparlamentet er studentenes høyeste interesseorgan ved NTNU i
    Gjøvik. Er det saker studenten ønsker å ta opp som gjelder skolen eller
    velferd, er det de man skal henvende seg til. De har møter med skolens
    ledelse ved jevne mellomrom hvor de tar opp saker fra studentene.
    Studentparlamentet har kontor i kantinen og kontortid i lunsjen mandag
    til fredag, men om man ser noen av medlemmene er det bare å spørre i vei.");
define("WORDING_STUDENTPARLAMENTET_CONTACT",
  "<p>
      Kontor i kantina i G-bygget, rom G157 <br>
      Åpent 11:15 - 13:00 mandag til fredag
    </p>
    <p>
      Send epost for studentpolitiske saker: <br>
      <a href='mailto:sp@gjøvik.ntnu.no'>sp@gjovik.ntnu.no</a>
      <br>
      tlf: 61 13 51 71
    </p>
    <p>
      Studentparlamentet ved NTNU i Gjøvik
      <br>
      Postboks 191
      <br>
      2802 Gjøvik
    </p>");

define("WORDING_IT_INFO",
  "Den sentrale IT-avdelingen ved NTNU blir kalt NTNU IT. Sentralt for deres
  virksomhet er spisskompetanse på utviklig, drift og feilhåndtering
  innnefor nettverk, serverdrift og applikasjonsdrift. Deres hovedoppgaver
  gjelder drift av felles- og basistjenster for NTNU, utvikle web-baserte
  løsninger for NTNU og være rådgiver til organisasjonen på IT-relaterte
  spørsmål.");
define("WORDING_IT_CONTACT",
  "<p>
    Henvendelse i skranke: <br>
    Kjelleren i K-bygget
  </p>
  <p>
    Send epost for IT-relaterte saker: <br>
    <a href='mailto:orakel@gjøvik.ntnu.no'>orakel@gjovik.ntnu.no</a> <br>
    tlf: 73 59 15 00
  </p>");

define("WORDING_VELFERD_INFO",
  "<p>
    Velferdsutvalget er et rådgivende organ som innstiller til Velferdstinget
    i Gjøvik, Ålsesund og Trondheim. Velferdstinget skal forvalte
    studentvelferden til alle studenter som betaler semesteravgift til
    Studentsamskipnaden i Gjøvik, Ålesund og Trondheim. Velferdsutvalget består
    av seks (6) studenter, valgt av SR-FI og Studentparlamentet ved NTNU
    Gjøvik. Disse studentene har direktekontakt med Studentsamskipnaden,
    kommunen og Velferdstinget sentralt og skal være represantivt for alle
    studentene i Gjøvik
  </p>
  <p>
    Velferdsutvalget har i mandat å representere Gjøvikstudentenes syn i alle
    velferdspolitiske saker. Velferdsuytvalget skal ivareta studentenes velferd
    i Gjøvik, da gjennom poitisk påvirkning, samarbeid med Sit og kontinuerlig
    videreutvikling av tilbud for studentene.
  </p>
  <p>
    Velferdsutvalget skal ta opp saker som angår den generelle velferden til
    studentene i Gjøvik. Dette er alle saker som angår studentlivet utenfor
    studiet. Eksempler på dette kan være saker som omhandler studentidret,
    kollektivtransport eller studentboliger.
  </p>");
define("WORDING_VELFERD_CONTACT",
  "Send epost for velferdsrelaterte saker: <br>
  <a href='mailto:au-gjovik@velferdstinget.no'>au-gjovik@velferdstinget.no</a>");

define("WORDING_SIT_INFO",
  "<p>
    Sit skal bidra til at studentene trives, og at flere ønsker å studere i Gjøvik. De skal utvikle attraktive velferdstilbud og varer og tjenester som studentene har behov for. De kan engasjerer seg i ekstern forretningsvirksomhet når dette bidrar til økt studentvelferd. Engasjerte ansatte, støtte til studentenes frivillighetskultur og et tett samarbeid med studentene og utdanningsinstitusjonene skal være grunnpilarene i deres arbeid.
  </p>
  <p>
    Sit er organisert i tre utøvende enheter: Sit Bolig, Sit Kafe og Sit Velferd, i tillegg til administrative fellestjenester.
    De har også datterselskaper og tilknyttede selskaper.
  </p>");
define("WORDING_SIT_CONTACT",
  "<p>
    Sentralbord <br>
    E-post: <a href='mailto:sit@sit.no'>sit@sit.no</a> <br>
    Åpent mandag–fredag 08.30–15.00
  </p>
  <p>
    Sit Bolig <br>
    E-post: <a href='mailto:sit@sit.no'>bolig@sit.no</a>
  </p>
  <p>
    Sit Kafe <br>
    E-post: <a href='mailto:kafe@sit.no'>kafe@sit.no</a> <br>
    Tlf: 73 59 32 50
  </p>
  <p>
    Sit Velferd <br>
    E-post: <a href='mailto:helse@sit.no'>helse@sit.no</a>
  </p>");
