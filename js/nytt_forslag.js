$(function(){
  //hiding elements on pageload
  $("#anonRadioDiv").hide();
  $("#titleDiv").hide();
  $("#suggTextDiv").hide();
  $("#submitBtn").hide();

  //show correct category info on based on value
  function js_show_cat_info(){

    $categoryValue = $("#selCat").val();

    switch ($categoryValue) {
      case "1": //IT
        $("#husdriftDiv").hide();
        $("#studParDiv").hide();
        $("#ITDiv").show();
        $("#velferdDiv").hide();
        $("#sitDiv").hide();
        break;
      case "2": //Drift
        $("#husdriftDiv").show();
        $("#studParDiv").hide();
        $("#ITDiv").hide();
        $("#velferdDiv").hide();
        $("#sitDiv").hide();
        break;
      case "3": //Velferd
        $("#husdriftDiv").hide();
        $("#studParDiv").hide();
        $("#ITDiv").hide();
        $("#velferdDiv").show();
        $("#sitDiv").hide();
        break;
      case "4": //Sit
        $("#husdriftDiv").hide();
        $("#studParDiv").hide();
        $("#ITDiv").hide();
        $("#velferdDiv").hide();
        $("#sitDiv").show();
        break;
      case "5": //StudPar
        $("#husdriftDiv").hide();
        $("#studParDiv").show();
        $("#ITDiv").hide();
        $("#velferdDiv").hide();
        $("#sitDiv").hide();
        break;
    }
  }

  function show_elements_based_on_contacted(){
    var $value = $('input[name=contacted]:checked:first', '#contactedRadio').val()
    if($value == '1') {
      $("#departmentWrapper").hide();
      $("#anonRadioDiv").show();
      $("#titleDiv").show();
      $("#suggTextDiv").show();
      $("#submitBtn").show();
    } else if ($value == '0') {
      $("#departmentWrapper").show();
      $("#anonRadioDiv").hide();
      $("#titleDiv").hide();
      $("#suggTextDiv").hide();
      $("#submitBtn").hide();
    }
  }

  //calling functions on pageload
  js_show_cat_info();
  show_elements_based_on_contacted();

  //checking for oninput events for different inputs
  $("#selCat").change(function(){
    js_show_cat_info()
  });

  //hiding and showing parts of the page depending on if the user has contacted the correct
  //department or not
  $("#contactedRadio").change(function(e){
    show_elements_based_on_contacted();
  });

});
