<?php
  // echo 'Veiledningssiden<br>';
?>


<div class="row"> <!-- Row for HUSDRIFT tittel start -->
  <div class="col-md-12 page-header">
    <h1><?php echo WORDING_DEPARTMENT_TITLE_HUSDRIFT; ?></h1>
  </div>
</div> <!-- Row for tittel slutt -->
<div class="row">
  <div class="col-md-8">
    <p><?php echo WORDING_HUSDRIFT_INFO; ?></p>
    <ul><?php echo WORDING_HUSDRIFT_LIST; ?></ul>
    <h1><small><?php echo WORDING_DEPARTMENT_CONTACT; ?></small></h1>
    <p><?php echo WORDING_HUSDRIFT_CONTACT; ?>
      <a href="http://lydiaweb.itea.ntnu.no/Lydia/demand/NewDemand.aspx">http://lydiaweb.itea.ntnu.no/Lydia/demand/NewDemand.aspx</a>
    </p>
  </div>
  <div class="col-md-4"></div>
</div>

<div class="row"> <!-- Row for STUDENTPARlAMENTET tittel start -->
  <div class="col-md-12 page-header">
    <h1><?php echo WORDING_DEPARTMENT_TITLE_STUDENTPARLAMENTET; ?></h1>
  </div>
</div> <!-- Row for tittel slutt -->
<div class="row">
  <div class="col-md-8">
    <p><?php echo WORDING_STUDENTPARLAMENTET_INFO; ?></p>
    <h1><small><?php echo WORDING_DEPARTMENT_CONTACT; ?></small></h1>
      <?php echo WORDING_STUDENTPARLAMENTET_CONTACT; ?>
  </div>
  <div class="col-md-4"></div>
</div>

<div class="row"> <!-- Row for IT-AVDELINGEN tittel start -->
  <div class="col-md-12 page-header">
    <h1><?php echo WORDING_DEPARTMENT_TITLE_IT; ?></h1>
  </div>
</div> <!-- Row for tittel slutt -->
<div class="row">
  <div class="col-md-8">
    <p><?php echo WORDING_IT_INFO; ?></p>
    <h1><small><?php echo WORDING_DEPARTMENT_CONTACT; ?></small></h1>
    <?php echo WORDING_IT_CONTACT; ?>
  </div>
  <div class="col-md-4"></div>
</div>

<div class="row"> <!-- Row for VELFERD tittel start -->
  <div class="col-md-12 page-header">
    <h1><?php echo WORDING_DEPARTMENT_TITLE_VELFERD; ?></h1>
  </div>
</div> <!-- Row for tittel slutt -->
<div class="row">
  <div class="col-md-8">
    <?php echo WORDING_VELFERD_INFO; ?>
    <h1><small><?php echo WORDING_DEPARTMENT_CONTACT; ?></small></h1>
    <p><?php echo WORDING_VELFERD_CONTACT; ?></p>
  </div>
  <div class="col-md-4"></div>
</div>

<div class="row"> <!-- Row for SIT tittel start -->
  <div class="col-md-12 page-header">
    <h1><?php echo WORDING_DEPARTMENT_TITLE_SIT; ?></h1>
  </div>
</div>  <!-- Row for tittel slutt -->
<div class="row">
  <div class="col-md-8">
    <?php echo WORDING_SIT_INFO; ?>
    <h1><small><?php echo WORDING_DEPARTMENT_CONTACT; ?></small></h1>
    <?php echo WORDING_SIT_CONTACT; ?>
  </div>
  <div class="col-md-4"></div>
</div>
