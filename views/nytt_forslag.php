<?php
require_once 'config/mysqli_config.php';
require_once 'libraries/library.php';

//declaring variables
$cat = '';
$title = '';
$fullTxt = '';
$contacted = '';
$anonymous = '';
$user = $_SESSION['user_id'];
$titleErr = '';
$suggTxtErr = '';

//functions for validating user inputs
function validateTitle($title, &$errorMsg) {
  if ($title == "") {
    $errorMsg = MESSAGE_NO_TITLE;
    return FALSE;
  } elseif ((strlen($title) < 5) || (strlen($title) > 120)) {
    $errorMsg = MESSAGE_TITLE_LENGTH;
    return FALSE;
  } else {
    return TRUE;
  }
}

function validateText($mainTxt, &$errorMsg) {
  if ($mainTxt == "") {
    $errorMsg = MESSAGE_NO_SUGGESTION_TEXT;
    return FALSE;
  } elseif (strlen($mainTxt) < 150) {
    $errorMsg = MESSAGE_SUGGESTION_LENGTH;
    return FALSE;
  } else {
    return TRUE;
  }
}

function validateContacted($contacted) {
  if ($contacted === '1') {
    return TRUE;
  } else {
    return FALSE;
  }
}

//funtion for creating the category select and storing user choice between creation attempts
function category_selects($category, $preservedValue) {
  while ($row = $category -> fetch_array(MYSQLI_ASSOC)) {
    if ($preservedValue === $row['category_id']) {
      echo "<option value='$row[category_id]' selected='selected'>$row[category_name]</option>";
    } else {
      echo "<option value='$row[category_id]'>$row[category_name]</option>";
    }
  }
}

//function for storing Contacted choice and showing correct selected
function show_contacted_radio($contacted) {
  if ($contacted === "1") {
    echo '<div class="radio" id="contactedRadio">
      <label class="radio-inline">
        <input id="contactedYes" type="radio" name="contacted" value="1" checked="checked"> Ja
      </label>
      <label class="radio-inline">
        <input id="contactedNo" type="radio" name="contacted" value="0"> Nei
      </label>
      <span id="departmentWrapper">
        <p>Du må kontakte riktig avdeling før du lager ett nytt forslag.</p>

        <span id="husdriftDiv">
          <div class="row"> <!-- Row for HUSDRIFT tittel start -->
            <div class="col-md-12 page-header">
              <h1>' . WORDING_DEPARTMENT_TITLE_HUSDRIFT . '</h1>
            </div>
          </div> <!-- Row for tittel slutt -->
          <div class="row">
            <div class="col-md-8">
              <p>' . WORDING_HUSDRIFT_INFO . '</p>
              <ul>' . WORDING_HUSDRIFT_LIST . '</ul>
              <h1><small>' . WORDING_DEPARTMENT_CONTACT . '</small></h1>
              <p>' . WORDING_HUSDRIFT_CONTACT . '
                <a href="http://lydiaweb.itea.ntnu.no/Lydia/demand/NewDemand.aspx">http://lydiaweb.itea.ntnu.no/Lydia/demand/NewDemand.aspx</a>
              </p>
            </div>
            <div class="col-md-4"></div>
          </div>
        </span>

        <span id="studParDiv">
          <div class="row"> <!-- Row for STUDENTPARlAMENTET tittel start -->
            <div class="col-md-12 page-header">
              <h1>' . WORDING_DEPARTMENT_TITLE_STUDENTPARLAMENTET . '</h1>
            </div>
          </div> <!-- Row for tittel slutt -->
          <div class="row">
            <div class="col-md-8">
              <p>' .  WORDING_STUDENTPARLAMENTET_INFO . '</p>
              <h1><small>' . WORDING_DEPARTMENT_CONTACT . '</small></h1>
                ' . WORDING_STUDENTPARLAMENTET_CONTACT . '
            </div>
            <div class="col-md-4"></div>
          </div>
        </span>

        <span id="ITDiv">
          <div class="row"> <!-- Row for IT-AVDELINGEN tittel start -->
            <div class="col-md-12 page-header">
              <h1>' . WORDING_DEPARTMENT_TITLE_IT . '</h1>
            </div>
          </div> <!-- Row for tittel slutt -->
          <div class="row">
            <div class="col-md-8">
              <p>' . WORDING_IT_INFO . '</p>
              <h1><small>' . WORDING_DEPARTMENT_CONTACT . '</small></h1>
              ' . WORDING_IT_CONTACT . '
            </div>
            <div class="col-md-4"></div>
          </div>
        </span>

        <span id="velferdDiv">
          <div class="row"> <!-- Row for VELFERD tittel start -->
            <div class="col-md-12 page-header">
              <h1>' . WORDING_DEPARTMENT_TITLE_VELFERD . '</h1>
            </div>
          </div> <!-- Row for tittel slutt -->
          <div class="row">
            <div class="col-md-8">
              ' . WORDING_VELFERD_INFO . '
              <h1><small>' . WORDING_DEPARTMENT_CONTACT . '</small></h1>
              <p>' . WORDING_VELFERD_CONTACT . '</p>
            </div>
            <div class="col-md-4"></div>
          </div>
        </span>

        <span id="sitDiv">
          <div class="row"> <!-- Row for SIT tittel start -->
            <div class="col-md-12 page-header">
              <h1>' . WORDING_DEPARTMENT_TITLE_SIT . '</h1>
            </div>
          </div>  <!-- Row for tittel slutt -->
          <div class="row">
            <div class="col-md-8">
              ' . WORDING_SIT_INFO . '
              <h1><small>' . WORDING_DEPARTMENT_CONTACT . '</small></h1>
              ' . WORDING_SIT_CONTACT . '
            </div>
            <div class="col-md-4"></div>
          </div>
        </span>


      </span>
    </div>';
  } else {
    echo '<div class="radio" id="contactedRadio">
      <label class="radio-inline">
        <input id="contactedYes" type="radio" name="contacted" value="1"> Ja
      </label>
      <label class="radio-inline">
        <input id="contactedNo" type="radio" name="contacted" value="0" checked="checked"> Nei
      </label>
      <span id="departmentWrapper">
        <p>Du må kontakte riktig avdeling før du lager ett nytt forslag.</p>

        <span id="husdriftDiv">
          <div class="row"> <!-- Row for HUSDRIFT tittel start -->
            <div class="col-md-12 page-header">
              <h1>' . WORDING_DEPARTMENT_TITLE_HUSDRIFT . '</h1>
            </div>
          </div> <!-- Row for tittel slutt -->
          <div class="row">
            <div class="col-md-8">
              <p>' . WORDING_HUSDRIFT_INFO . '</p>
              <ul>' . WORDING_HUSDRIFT_LIST . '</ul>
              <h1><small>' . WORDING_DEPARTMENT_CONTACT . '</small></h1>
              <p>' . WORDING_HUSDRIFT_CONTACT . '
                <a href="http://lydiaweb.itea.ntnu.no/Lydia/demand/NewDemand.aspx">http://lydiaweb.itea.ntnu.no/Lydia/demand/NewDemand.aspx</a>
              </p>
            </div>
            <div class="col-md-4"></div>
          </div>
        </span>

        <span id="studParDiv">
          <div class="row"> <!-- Row for STUDENTPARlAMENTET tittel start -->
            <div class="col-md-12 page-header">
              <h1>' . WORDING_DEPARTMENT_TITLE_STUDENTPARLAMENTET . '</h1>
            </div>
          </div> <!-- Row for tittel slutt -->
          <div class="row">
            <div class="col-md-8">
              <p>' .  WORDING_STUDENTPARLAMENTET_INFO . '</p>
              <h1><small>' . WORDING_DEPARTMENT_CONTACT . '</small></h1>
                ' . WORDING_STUDENTPARLAMENTET_CONTACT . '
            </div>
            <div class="col-md-4"></div>
          </div>
        </span>

        <span id="ITDiv">
          <div class="row"> <!-- Row for IT-AVDELINGEN tittel start -->
            <div class="col-md-12 page-header">
              <h1>' . WORDING_DEPARTMENT_TITLE_IT . '</h1>
            </div>
          </div> <!-- Row for tittel slutt -->
          <div class="row">
            <div class="col-md-8">
              <p>' . WORDING_IT_INFO . '</p>
              <h1><small>' . WORDING_DEPARTMENT_CONTACT . '</small></h1>
              ' . WORDING_IT_CONTACT . '
            </div>
            <div class="col-md-4"></div>
          </div>
        </span>

        <span id="velferdDiv">
          <div class="row"> <!-- Row for VELFERD tittel start -->
            <div class="col-md-12 page-header">
              <h1>' . WORDING_DEPARTMENT_TITLE_VELFERD . '</h1>
            </div>
          </div> <!-- Row for tittel slutt -->
          <div class="row">
            <div class="col-md-8">
              ' . WORDING_VELFERD_INFO . '
              <h1><small>' . WORDING_DEPARTMENT_CONTACT . '</small></h1>
              <p>' . WORDING_VELFERD_CONTACT . '</p>
            </div>
            <div class="col-md-4"></div>
          </div>
        </span>

        <span id="sitDiv">
          <div class="row"> <!-- Row for SIT tittel start -->
            <div class="col-md-12 page-header">
              <h1>' . WORDING_DEPARTMENT_TITLE_SIT . '</h1>
            </div>
          </div>  <!-- Row for tittel slutt -->
          <div class="row">
            <div class="col-md-8">
              ' . WORDING_SIT_INFO . '
              <h1><small>' . WORDING_DEPARTMENT_CONTACT . '</small></h1>
              ' . WORDING_SIT_CONTACT . '
            </div>
            <div class="col-md-4"></div>
          </div>
        </span>


      </span>
    </div>';
  }
}

//function for storing Anonymous choice and showing correct selected
function show_anonymous_radio($anonymous) {
  if ($anonymous === "1") {
    echo '<div class="radio">
      <label class="radio-inline">
        <input type="radio" name="anonymous" id="anonymt1" value="0"> Ja
      </label>
      <label class="radio-inline">
        <input type="radio" name="anonymous" id="anonymt2" value="1" checked="checked"> Nei
      </label>
    </div>';
  } else {
    echo '<div class="radio">
      <label class="radio-inline">
        <input type="radio" name="anonymous" id="anonymt1" value="0" checked="checked"> Ja
      </label>
      <label class="radio-inline">
        <input type="radio" name="anonymous" id="anonymt2" value="1"> Nei
      </label>
    </div>';
  }
}

//querying db for category info
$query = "SELECT * FROM suggestion_category";
$resultCat = $db->query($query);
if (!$resultCat) die(MESSAGE_TECHNICAL_DB_PROBLEM);

//processing the new suggestiong
if ($login->isUserLoggedIn() && isset($_POST['createSugg'])) {

  $cat = get_post('category', $db);
  $title = get_post('title', $db);
  $fullTxt = get_post('suggestionTxt', $db);
  $contacted = get_post('contacted', $db);
  $anonymous = get_post('anonymous', $db);

  if (  !(validateTitle($title, $titleErr)) ||
        !(validateText($fullTxt, $suggTxtErr)) ||
        !(validateContacted($contacted))) {
    //Faults with the inputs, error messages allready handled by validate functions
    //so do nothing here
  } else {
    //inputs validated, continuing processing new suggestion
    $query = "INSERT INTO suggestions(title, suggestion_text, category, author, post_anonymous)
              VALUES ('$title', '$fullTxt', '$cat', '$user', '$anonymous')";
    $result = $db->query($query);
    if (!$result) die(MESSAGE_TECHNICAL_DB_PROBLEM);

    //updating the suggestion_user_votes table
    //selecting highest value of suggestion_id in suggestion, where user is the author
    $query = "SELECT MAX(suggestion_id)
              FROM suggestions
              WHERE author = $user";
    $result = $db->query($query);
    if (!$result) die(MESSAGE_TECHNICAL_DB_PROBLEM);

    $sugg_id = $result -> fetch_array(MYSQLI_ASSOC);
    $sugg_id = implode($sugg_id);

    $query = "INSERT INTO suggestion_user_votes VALUES ('$sugg_id', '$user')";
    $result = $db->query($query);
    if (!$result) die(MESSAGE_TECHNICAL_DB_PROBLEM);

    $resultCat->close();
    $db->close();
    echo '<div class="page-header">
    <h1>'; echo MESSAGE_NEW_SUGGESTION; echo '</h1>
    </div>
    <div class="row">
      <div class="col-md-12">
        <h4>';echo MESSAGE_SUGGESTION_SAVED; echo '</h4>
      </div>
    </div>';
    exit;
  }
}

?>
<div class="page-header">
  <h1><?php echo MESSAGE_NEW_SUGGESTION; ?></h1>
</div>
<div class="row">
  <div class="col-md-7">
    <form role="form" class="form-signin" action="?nytt_forslag" method="POST">

      <div class="form-group">
        <label for="selCat"><?php echo WORDING_CHOOSE_CATEGORY; ?></label>
        <select class="form-control" name="category" id="selCat">
          <?php category_selects($resultCat, $cat); ?>
        </select>
      </div>

      <div class="form-group">
        <label>
          <?php echo MESSAGE_CORRECT_DEPARTMENT_CONTACTED; ?>
        </label>
        <?php show_contacted_radio($contacted) ?>

      </div>




      <div class="form-group" id="anonRadioDiv">
        <label>
          <?php echo MESSAGE_ANONYMOUS_POSTING; ?>
        </label>
        <?php show_anonymous_radio($anonymous) ?>
      </div>

      <div class="form-group" id="titleDiv">
        <label for="title"><?php echo WORDING_SUGGESTION_TITLE; ?></label>
        <input type="text" name="title" class="form-control" id="title" placeholder="Tittel" value="<?php echo $title; ?>" required>
        <div class="text-center" id="titleErr">
          <?php echo $titleErr; ?>
        </div>
      </div>
      <div class="form-group" id="suggTextDiv">
        <label for="suggestionTxt"><?php echo WORDING_SUGGESTION_TEXT; ?></label>
        <textarea class="form-control" rows="6" name="suggestionTxt" id="suggestionTxt"><?php echo $fullTxt; ?></textarea>
        <div class="text-center" id="suggTxtErr">
          <?php echo $suggTxtErr; ?>
        </div>

      </div>
      <button type="submit" id="submitBtn" class="btn btn-primary" name="createSugg"><?php echo WORDING_SUGGESTION_SUBMIT; ?></button>
    </form>
  </div>
<div class="col-md-5"></div>
</div>

<script src="js/nytt_forslag.js" type="text/javascript"></script>
<?php
$resultCat->close();
$db->close();
?>
