<?php error_reporting(0);
require_once 'config/mysqli_config.php';
require_once 'libraries/library.php';
$q = $_REQUEST['q'];

//when the Vote/Unvote button is clicked
if ($login->isUserLoggedIn() && isset($_POST['voteBtn'])) {
  $voteStatus = get_post('voteBoolean', $db);
  $suggId = get_post('suggId', $db);
  $user = $_SESSION['user_id'];

  //if user is voting on suggestion
  if ($voteStatus === '0') {
    $query = "INSERT INTO suggestion_user_votes(suggestion, user)
              VALUES ('$suggId', '$user')";
    $result = $db->query($query);
    if (!$result) die(MESSAGE_TECHNICAL_DB_PROBLEM);

    //updating this suggestions votes-counter in the suggestions table
    $query = "UPDATE suggestions SET votes = votes + 1
              WHERE suggestion_id = '$suggId'";
    $result = $db->query($query);
    if (!$result) die(MESSAGE_TECHNICAL_DB_PROBLEM);

  //if user is removing vote from suggestion
} elseif ($voteStatus === '1') {
    $query = "DELETE FROM suggestion_user_votes
              WHERE suggestion = '$suggId'
              AND user = '$user'
              ";
    $result = $db->query($query);
    if (!$result) die(MESSAGE_TECHNICAL_DB_PROBLEM);

    //updating this suggestions votes-counter in the suggestions table
    $query = "UPDATE suggestions SET votes = votes - 1
              WHERE suggestion_id = '$suggId'";
    $result = $db->query($query);
    if (!$result) die(MESSAGE_TECHNICAL_DB_PROBLEM);
  }
}

//gather suggestion data from DB
$query = "SELECT s.suggestion_id, s.title, st.status_name, s.suggestion_text, s.votes, u.user_name, s.post_anonymous, s.over_max_reported, s.author, s.created_at
          FROM suggestions s, users u, suggestion_status st
          WHERE u.user_id = s.author
          AND s.suggestion_text LIKE '%$q%'
          OR s.title LIKE '%$q%'
          GROUP BY s.suggestion_id
          ORDER BY suggestion_id DESC
          ";
$resultSugg = $db->query($query);
if (!$resultSugg) die(MESSAGE_TECHNICAL_DB_PROBLEM);

function show_votes_correct_votebutton($login, $suggestion, $db) {

  //show correct button based on login status and voting history for a suggestion
  if (!empty($_SESSION['user_name']) && $login->isUserLoggedIn()) {

    //querying db to see if user has voted on the suggestion
    $query = "SELECT * FROM suggestion_user_votes
              WHERE $_SESSION[user_id] = user
              AND $suggestion[suggestion_id] = suggestion
              ";
    $votedSugg = $db->query($query);
    if (!$votedSugg) die(MESSAGE_TECHNICAL_DB_PROBLEM);

    if ($_SESSION['user_id'] == $suggestion['author']) {
      //if user is the author of article
      echo '<p class="text-center text-info">';
      echo MESSAGE_SUGGESTION_AUTHOR;
      echo '</p>';

    } elseif ( mysqli_num_rows($votedSugg) !== 0 ) {
      //if user has voted
      echo '
      <form class="" action="?forslag" method="POST">
        <input type="hidden" name="suggId" value="'.$suggestion['suggestion_id'].'"></input>
        <input type="hidden" name="voteBoolean" value="1"></input>
        <input class="btn btn-primary btn-lg btn-block" type="submit" name="voteBtn" value="'. WORDING_UNVOTE_BUTTON .'">
      </form>';
    } else {
      //if user has not voted yet
      echo '
      <form class="" action="?forslag" method="POST">
        <input type="hidden" name="suggId" value="'.$suggestion['suggestion_id'].'"></input>
        <input type="hidden" name="voteBoolean" value="0"></input>
        <input class="btn btn-primary btn-lg btn-block" type="submit" name="voteBtn" value="'. WORDING_VOTE_BUTTON .'">
      </form>';
    }
    $votedSugg->close();

  } else {
    echo '
    <form class="" action="?login" method="POST">
      <input class="btn btn-primary btn-lg btn-block" type="submit" name="voteBtn" value="'. WORDING_VOTE_BUTTON .'">
    </form>';
  }
}

//function for showing the suggestions
function show_suggestions($suggestions, $login, $db) {
  if ( mysqli_num_rows($suggestions) === 0 ) {
    //if there are no suggestions in the system
    echo MESSAGE_NO_SUGGESTIONS;
  } else {
    while ($row = $suggestions -> fetch_array(MYSQLI_ASSOC)) {
      if ($row['over_max_reported'] == 1) {
        //do nothing
      } else {
        //Suggestions information
        echo '
          <div class="row">
            <div class="col-sm-8 col-xs-8">
              <h3><span>'.$row['title'].'</span> <small><span id="status" class="text-success">'.$row['status_name'].'</span></small></h3>
              <p>'.$row['suggestion_text'].'</p>
              <p><small>Lagt til: '.$row['created_at'].'</small></p>';
                 /* check user preference for showing name
                if ($row['post_anonymous'] == 1) {
                  echo WORDING_SUGGESTION_BY . '<span>'.$row['user_name'].'</span>';
                } else {
                  echo WORDING_SUGGESTION_BY_ANONYMOUS;
              }</small></p>*/
            echo '</div>

            <div class="col-sm-2 col-xs-4">
              <h3 class="text-center">'.
                $row['votes']
              .'</h3>
              <p class="text-center">';
                echo WORDING_SUGGESTION_VOTES;
              echo '</p>';

            show_votes_correct_votebutton($login, $row, $db);

            echo '
            </div>
            <div class="col-sm-2 col-xs-0"></div>
          </div><!-- /.row -->
        ';
      }
    }
  }
}

?>
<div class="page-header">
  <div class="row">
    <div class="col-md-8">
      <h1><?php echo WORDING_SUGGESTION_HEADER; ?></h1>
    </div>

    <div class="col-md-2">
      <a class="btn btn-primary btn-lg btn-block" href="index.php?nytt_forslag" role="button"><?php echo WORDING_ADD_NEW_SUGGESTION; ?></a>
    </div>
    <div class="col-md-2">
    </div>
  </div>


</div>

<!-- Search box -->
<div class="row">

  <div class="col-md-8">
    <form method="get">
      <div class="form-group has-feedback">
        <input type="hidden" name="forslag">
        <input type="search" name="q" class="form-control input-lg" id="search" placeholder=<?php echo WORDING_SEARCH_PLACEHOLDER; ?> value="<?php if ($q != null) { echo $q; } ?>">
        <span class="glyphicon glyphicon-search form-control-feedback" aria-hidden="true"></span>
        <span id="inputSuccess2Status" class="sr-only"><?php echo MESSAGE_SUCCESS; ?></span>
      </div>
    </form>
  </div>
  <div class="col-md-4"></div>
</div>

<?php
show_suggestions($resultSugg, $login, $db);
$resultSugg->close();
$db->close();
?>
