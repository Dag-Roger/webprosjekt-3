<div class="page-header">
	<h1><?php echo WORDING_LOGIN; ?></h1>
</div>
<div class="row">
	<div class="col-md-5">
	  <form method="post" action="<?php echo $_SERVER['SCRIPT_NAME']; ?>">
				<label for="user_name"><?php echo 'NTNU e-post'; ?></label>
            <div class="input-group">
            	<input id="user_name" type="text" class="form-control" name="user_name" required autofocus />
                <span class="input-group-addon" id="basic-addon2">@stud.ntnu.no</span>
			</div>
			<div class="form-group">
				<label for="user_password"><?php echo WORDING_PASSWORD; ?></label>
				<input id="user_password" type="password" class="form-control" name="user_password" autocomplete="off" required /><br/>
			</div>
			<div class="checkbox">
		    <label>
		      	<input type="checkbox" id="user_rememberme" name="user_rememberme" value="1" /> <?php echo WORDING_REMEMBER_ME; ?>
		    </label>
		  </div>
			<input type="submit" class="btn btn-primary" name="login" value="<?php echo WORDING_LOGIN; ?>" />
		</form>
		<p class="pull-right">
			<?php echo (ALLOW_USER_REGISTRATION ? '<a href="?register">'. WORDING_REGISTER_NEW_ACCOUNT .'</a>&nbsp;&nbsp;|&nbsp;&nbsp;' : ''); ?>
			<a href="?password_reset"><?php echo WORDING_FORGOT_MY_PASSWORD; ?></a>
		</p>
	</div>
	<div class="col-md-7"></div>
</div>
