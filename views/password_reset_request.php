<div class="page-header">
	<h1><?php echo WORDING_FORGOT_MY_PASSWORD; ?></h1>
</div>
<div class="row">
	<div class="col-md-5">
	  <form method="post" action="?password_reset">

			<div class="form-group">
				<label for="user_name"><?php echo WORDING_REQUEST_PASSWORD_RESET; ?></label>
				<input id="user_name" class="form-control" type="text" name="user_name" required autofocus />
			</div>

			<input type="submit" class="btn btn-primary" name="request_password_reset" value="<?php echo WORDING_RESET_PASSWORD; ?>" />
		</form>
		<a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>"><?php echo WORDING_BACK_TO_LOGIN; ?></a>
	</div>
	<div class="col-md-7"></div>
</div>
