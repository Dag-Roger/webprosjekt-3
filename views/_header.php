<!DOCTYPE html>
<html lang="no">
<head><!-- Begin head section -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>NTNU Underskriftkampanjer</title> <!-- TODO:TEMP TITLE -->

  <!-- jQuery -->
  <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
  <!-- Bootstrap -->
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <!-- Optional theme -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

  <link rel="stylesheet" href="css/style.css"><!-- custom CSS -->
</head><!-- End head section -->
<body>
  <?php

    //showing the correct navbar based on session information
    /*	partial code for another usertype(mods)
  	if (isset($_SESSION['isloggedin']) &&  $_SESSION['usertype'] == 1 ){
      require_once 'navbarAdmin.php';
    } elseif
  	*/
  	if (!empty($_SESSION['user_name']) && $login->isUserLoggedIn()) {
      require_once 'nav_loggedin.php';
    } else {
      require_once 'nav_guest.php';
    }
  ?>

  <!-- Main container -->
  <div class="container">


<!--these are closed in footer.php -->
<?php
// show potential errors / feedback (from login object)
if (isset($login)) {
    foreach ($login->errors as $error) {
        echo "<div><span class=\"login_error\">$error</span></div><br/>\n";
    }

    foreach ($login->messages as $message) {
        echo "<div><span class=\"login_message\">$message</span></div><br/>\n";
    }
}
?>
