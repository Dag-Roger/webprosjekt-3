<?php

?>
<!-- Nav guest -->
<nav class="navbar navbar-default navbar-static-top">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <a class="navbar-brand" href="index.php?forslag">NTNU Underskriftkampanjer</a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
          <li><a href="index.php?veiledning"><?php echo WORDING_NAVIGATION_GUIDANCE; ?></a></li>
          <li><a href="index.php?login"><span class="glyphicon glyphicon-user"></span> <?php echo WORDING_NAVIGATION_REGISTER_LOGIN; ?></a></li>
      </ul>
  </div><!-- /.navbar-collapse -->
  </div><!-- /.container -->
</nav>
