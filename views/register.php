<div class="page-header">
	<h1><?php echo WORDING_REGISTER_NEW_ACCOUNT; ?></h1>
</div>
<div class="row">
	<div class="col-md-5">
	  <form method="post" action="?register">

			<label for="user_email"><?php echo WORDING_USER_EMAIL; ?></label>
			<div class="input-group">
			  <input id="user_name" type="text" class="form-control" pattern="[a-zA-Z0-9.]{2,64}" name="user_name" required autofocus autocomplete="off" />
			  <span class="input-group-addon" id="basic-addon2">@stud.ntnu.no</span>
			</div>

			<div class="form-group">
				<label for="user_password_new"><?php echo WORDING_REGISTRATION_PASSWORD; ?></label>
        <input id="user_password_new" type="password" name="user_password_new" pattern=".{6,}" required autocomplete="off" class="form-control"/>
			</div>
      <input type="submit" name="register" class="btn btn-primary" value="<?php echo WORDING_REGISTER; ?>" />
    </form>
		<p class="pull-right">
			<?php echo (ALLOW_USER_REGISTRATION ? '<a href="?login">'. WORDING_LOGIN .'</a>&nbsp;&nbsp;|&nbsp;&nbsp;' : ''); ?>
			<a href="?password_reset"><?php echo WORDING_FORGOT_MY_PASSWORD; ?></a>
		</p>
	</div>
	<div class="col-md-7"></div>
</div>
