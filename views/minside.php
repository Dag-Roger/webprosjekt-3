<?php
require_once 'config/mysqli_config.php';

$userId = $_SESSION['user_id'];

function show_suggestions_user($suggestions) {
  if ( mysqli_num_rows($suggestions) === 0 ) {
    //if the user has created no suggestion
    echo MESSAGE_NO_USER_SUGGESTIONS;
  } else {
    while ($row = $suggestions -> fetch_array(MYSQLI_ASSOC)) {

    //Suggestions information
    echo '
      <div class="row">
        <div class="col-sm-8 col-xs-8">
          <h3><span id="suggestionTitle">'.$row['title'].'</span> <small><span id="status">'.$row['status_name'].'</span></small></h3>
          <p id="suggestionText">'.$row['suggestion_text'].'</p>
          <p><small>'; echo WORDING_SUGGESTION_BY; echo '<span id="username">'; echo WORDING_YOUR_SUGGESTIONS_AUTHOR; echo '</span></small></p>
        </div>

        <div class="col-sm-2 col-xs-4">
          <h3 class="text-center" id="numberOfVotes">'.
            $row['votes']
          .'</h3>
          <p class="text-center">';
          echo  WORDING_SUGGESTION_VOTES;
          echo '</p>
        </div>
        <div class="col-sm-2 col-xs-0"></div>
      </div><!-- /.row -->
    ';
    }
  }
}

echo '
<div class="page-header">
  <h1>'; echo MESSAGE_YOUR_SUGGESTIONS; echo '</h1>
</div>';

//Querying for this users suggestions.
$query = "SELECT s.title, s.suggestion_text, s.votes, u.user_name, st.status_name
          FROM suggestions s, users u, suggestion_status st
          WHERE u.user_id = s.author
          AND s.status = st.status_id
          AND s.author = '$userId'
          ORDER BY suggestion_id DESC";
$resultSugg = $db->query($query);
if (!$resultSugg) die(MESSAGE_TECHNICAL_DB_PROBLEM);


//building the pageoutput
show_suggestions_user($resultSugg);

//closing db connections
$resultSugg->close();
$db->close();
?>
