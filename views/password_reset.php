<div class="page-header">
	<h1><?php echo WORDING_FORGOT_MY_PASSWORD; ?></h1>
</div>
<div class="row">
	<div class="col-md-5">
	  <form method="post" action="?password_reset">
			<input type='hidden' name='user_name' value='<?php echo $_REQUEST['user_name']; ?>' />
			<input type='hidden' name='verification_code' value='<?php echo $_REQUEST['verification_code']; ?>' />

			<div class="form-group">
				<label for="user_password_new"><?php echo WORDING_NEW_PASSWORD; ?></label>
				<input id="user_password_new" class="form-control" type="password" name="user_password_new" pattern=".{6,}" required autocomplete="off" autofocus/>
			</div>
			<div class="form-group">
				<label for="user_password_repeat"><?php echo WORDING_NEW_PASSWORD_REPEAT; ?></label>
				<input id="user_password_repeat" class="form-control" type="password" name="user_password_repeat" pattern=".{6,}" required autocomplete="off" />
			</div>

			<input type="submit" class="btn btn-primary" name="submit_new_password" value="<?php echo WORDING_SUBMIT_NEW_PASSWORD; ?>" />
		</form>
		<a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>"><?php echo WORDING_BACK_TO_LOGIN; ?></a>
	</div>
	<div class="col-md-7"></div>
</div>
